import axios from "axios";
const apiUrl = import.meta.env.VITE_API_URL;

const clientWithHeader = axios.create({
  baseURL: apiUrl,
  headers: {
    'Content-Type': 'application/json'
  },
});

// Request interceptor
clientWithHeader.interceptors.request.use(
    config => {
        config.headers['Authorization'] = `Bearer ` + localStorage.getItem('token')
      return config;
    },
    error => {
      console.error('Request interceptor error:', error);
      return Promise.reject(error);
    }
);

// Response interceptor
clientWithHeader.interceptors.response.use(
    response => {
      return response;
    },
    error => {
      // Handle response error
      if(error.response.status === 401){
          console.log("token expired.")
          localStorage.removeItem('token');
          window.location.href = '/login';
      }
      return Promise.reject(error);
    }
);

export default clientWithHeader;