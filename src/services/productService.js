import clientWithHeader from "../config/apiSetting";

export const getAllProducts = async () => {
  try {
    const res = await clientWithHeader.get("/product/all");
    if (res.status == 200) {
      return res.data;
    }
  } catch (error) {
    console.log(error.message);
  }
};
