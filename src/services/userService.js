export const authorizedUser = async () => {
    try {
        const token = localStorage.getItem('token');
        if(token){
            return true
        } else {
            return false
        }
    } catch (error) {
        console.log(error.message);
    }
};
