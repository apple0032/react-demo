import Grid from "@mui/material/Grid";
import * as React from "react";
import '../chart.css'
import {useNavigate} from "react-router-dom";
import clientWithHeader from "../../src/config/apiSetting"
import CircularProgress from '@mui/material/CircularProgress';

import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import {useState} from "react";
import Loader from "../component/ui/Loader.jsx";

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

export const options = {
    responsive: true,
    plugins: {
        legend: {
            position: 'top',
        },
        title: {
            display: true,
            text: 'Chart of Avg. EUI By Property Type',
        },
    },
};

const ChartPage = () => {

    let labels = []
    let eui_date = []

    const [isLoading, setIsLoading] = useState(true);
    const navigate = useNavigate();
    const handleClick = (url) => {
        navigate(url);
    }
    const Logout = () => {
        localStorage.removeItem('token');
        window.location.href = '/login';
    };

    const [chartData, setChartData] = useState({
        labels,
        datasets: [
            {
                label: 'Average EUI',
                data: [],
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
            }
        ],
    });

    React.useEffect(() => {
        (async () => {
            let result = await clientWithHeader.get('/building/avgeui');
            if(result.status && result.data.length >0){
                (result.data).forEach((el) => {
                    labels.push(el.type)
                    eui_date.push(el.average_eui)
                })
                setChartData({
                    labels,
                    datasets: [
                        {
                            label: 'Average EUI',
                            data: eui_date,
                            backgroundColor: 'rgba(255, 99, 132, 0.5)',
                        }
                    ],
                })
                setIsLoading(false)
            }
            return result;
        })();
    }, []);

  return (
  <div className="chart-container">
        <Grid container spacing={2}>
            <Grid item xs={7} md={7}>
                <div className="details-title">
                    <span>Avg. EUI By Property Type</span>
                    {isLoading && <CircularProgress />}
                </div>
            </Grid>
            <Grid item xs={3} md={3}>
                <div className="select-page-container">
                    <span onClick={() => handleClick('/')}>OVERVIEW</span>
                </div>
                <div className="select-page-container">
                    <span onClick={() => handleClick('/chart')}>CHARTS</span>
                </div>
                <div className="select-page-container">
                    <div className="logout-btn" onClick={() => Logout()}>SIGN OUT</div>
                </div>
            </Grid>
        </Grid>
      <Grid container spacing={2}>
          <Grid item xs={12} md={12}>
              <div className="chart">
                  <Bar options={options} data={chartData} />
              </div>
          </Grid>
      </Grid>
    </div>
  );
};

export default ChartPage;
