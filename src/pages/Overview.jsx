import * as React from 'react';
import {Navigate, useNavigate} from "react-router-dom";
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import HomeWorkIcon from '@mui/icons-material/HomeWork';
import ListItemText from '@mui/material/ListItemText';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';
import DetailsBox from "../component/ui/DetailsBox.jsx";
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import {useState} from "react";
import clientWithHeader from "../../src/config/apiSetting"
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import PrimaryPropertyType from  '../model/PrimaryPropertyType.json'
import CircularProgress from '@mui/material/CircularProgress';

export default function CheckboxList() {
    const per_page = 14 //Each building show in one page
    const [isLoading, setIsLoading] = useState(true);
    const [isLoggedIn, setIsLoggedIn] = useState(true);
    const [totalPage, setTotalPage] = useState(1);
    const [totalResult, setTotalResult] = useState(0);
    let [buildings, setbuildings] = React.useState([]);
    const [propertyType, setPropertyType] = React.useState('all');
    let [detaisObject, setDetailsObject] = React.useState({
        "OSEBuildingID": "",
        "DataYear": "",
        "BuildingType": "",
        "PrimaryPropertyType": "",
        "PropertyName": "",
        "Address": "",
        "City": "",
        "State": "",
        "ZipCode": "",
        "TaxParcelIdentificationNumber": "",
        "CouncilDistrictCode": "",
        "Latitude": 47.60755,
        "Longitude": -122.33244,
        "YearBuilt": "",
        "NumberofBuildings": "",
        "NumberofFloors": ""
    });
    const [inputPage, setInputPage] = useState(1);
    const [inputSearch, setInputSearch] = useState("");

    const handleKeyPress = async(event) => {
        if (event.key === 'Enter') {
            if(inputPage > 0 && inputPage <=  totalPage) {
                setInputPage(inputPage);
                setPage(+(inputPage));
                let result = await getData(per_page, inputPage);
                setbuildings(result.data.rows)
            } else {
                alert('Please input from 1 to ' + totalPage)
            }
        }
    };

    const handleSearch = async(event) => {
        if (event.key === 'Enter') {
            if(inputSearch !== "") {
                //console.log(inputSearch)
                setPage(1);
                setInputPage(1);
                let result = await getData(per_page, 1, inputSearch,propertyType);
                setTotalPage( +(result.data.num_of_page) )
                setbuildings(result.data.rows)
            } else {
                await onClickResetSearch()
            }
        }
    };

    async function onClickResetSearch(){
        setInputSearch("")
        let result = await getData(per_page, 1);
        setTotalPage( +(result.data.num_of_page) )
        setbuildings(result.data.rows)
        setPropertyType("all")
    }


    const getData = async (per_page, page_num, PropertyName = null, PropertyType = null) => {
        setIsLoading(true)
        let searchUrl = `/building/find?per_page=${per_page}&page_num=${page_num}`;
        if (PropertyName !== null && PropertyName !== "") {
            searchUrl = searchUrl + '&PropertyName=' + PropertyName; // Corrected line
        }
        if (PropertyType !== null && PropertyType !== "" && PropertyType !== "all") {
            searchUrl = searchUrl + '&PrimaryPropertyType=' + PropertyType; // Corrected line
        }
        let result = await clientWithHeader.get(searchUrl);
        setTotalResult(result.data.count)
        setIsLoading(false)
        return result;
    };


    React.useEffect(() => {
        (async ()=>{
            let result = await getData(per_page, 1);
            setTotalPage( +(result.data.num_of_page) )
            setbuildings(result.data.rows)
        })()
        /*
            async function fetchData() {
                let result = await getData(per_page, 1);
                console.log(result.data.rows);
                setbuildings(result.data.rows);
              }
              fetchData();
        */
    }, []);

    React.useEffect(() => {
        if(buildings.length > 0){
            setDetailsObject(buildings[0])
        }
    }, [buildings]);

    const [page, setPage] = React.useState(1);
    const handleChange = async(event,value) => {
        setPage(value);
        setInputPage(value);
        let result = await getData(per_page, value);
        setbuildings(result.data.rows)
    };

    const handleSelect = async(event,value) => {
        setPropertyType(value.props.value)
        let result = await getData(per_page, 1,inputSearch, value.props.value);
        setPage(1);
        setInputPage(1);
        setbuildings(result.data.rows)
        setTotalPage(result.data.num_of_page)
    };


    function onClickDetails(obj) {
        setDetailsObject(obj)
    }

    const Logout = () => {
        localStorage.removeItem('token');
        setIsLoggedIn(false);
    };

    const navigate = useNavigate();
    const handleClick = (url) => {
        navigate(url);
    }

    if (!isLoggedIn ) {
        return <Navigate to="/login" />;
    }

    return (
        <div className="overview-container">
            <Grid container spacing={2}>
                <Grid item xs={7} md={7}>
                    <div className="details-title">
                        <span>Building Information</span> {isLoading && <CircularProgress />}
                    </div>
                </Grid>
                <Grid item xs={5} md={5}>
                    <div className="select-page-container">
                        <span onClick={() => handleClick('/')}>OVERVIEW</span>
                    </div>
                    <div className="select-page-container">
                        <span onClick={() => handleClick('/chart')}>CHARTS</span>
                    </div>
                    <div className="select-page-container">
                        <div className="logout-btn" onClick={() => Logout()}>SIGN OUT</div>
                    </div>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12} md={8}>
                    <div className="details-container">
                        <DetailsBox data={detaisObject} />
                    </div>
                </Grid>
                <Grid item xs={12} md={4}>
                    <div className="search-building-box">
                        <input
                            type="text"
                            value={inputSearch}
                            onChange={(event) => setInputSearch(event.target.value)}
                            placeholder="Search building name keyword."
                            onKeyPress={handleSearch}
                        />
                        <div className="search-building-reset" onClick={() => onClickResetSearch()}><RestartAltIcon /></div>
                    </div>
                    <div className="list-container">
                        <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
                            {buildings.map((object) => {
                                let labelId = `building-list-label-${object.OSEBuildingID}`;
                                return (
                                    <ListItem
                                        key={object.OSEBuildingID}
                                        secondaryAction={
                                            <IconButton edge="end" aria-label="comments">
                                                <HomeWorkIcon />
                                            </IconButton>
                                        }
                                        disablePadding
                                        onClick={() => onClickDetails(object)}
                                        className={object.OSEBuildingID === detaisObject.OSEBuildingID ? "building-list-clicked" : ""}
                                    >
                                        <ListItemButton role={undefined} dense>
                                            <ListItemText id={labelId} primary={`${object.PropertyName}`} />
                                        </ListItemButton>
                                    </ListItem>
                                );
                            })}
                        </List>
                    </div>
                </Grid>
                <Grid item xs={12} md={3}>
                    <div className="selection-container">
                        <Box sx={{ minWidth: 120 }}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">Property Type</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={propertyType}
                                    label="Property Type"
                                    onChange={handleSelect}
                                >
                                    <MenuItem value={"all"}>All</MenuItem>
                                    {PrimaryPropertyType.map((element) => (
                                        <MenuItem key={element} value={element}>{element}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Box>
                    </div>
                </Grid>
                <Grid item xs={12} md={7}>
                    <div className="pagination-container">
                        <Stack spacing={1}>
                            <Typography>Page: {page} , Totals: {totalResult}</Typography>
                            <Pagination count={totalPage} page={page} boundaryCount={3} onChange={handleChange} color="primary" variant="outlined" shape="rounded" size="large" showFirstButton showLastButton/>
                        </Stack>
                        <div className="custom_page">
                            Current page :
                            <input
                                type="text"
                                value={inputPage}
                                onChange={(event) => setInputPage(event.target.value)}
                                onKeyPress={handleKeyPress}
                            />
                            <span className="custom_message">(Press enter to submit)</span>
                        </div>
                    </div>
                </Grid>
                <br/>
            </Grid>
        </div>
    );
}
