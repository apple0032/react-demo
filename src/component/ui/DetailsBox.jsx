import HomeWorkIcon from "@mui/icons-material/HomeWork.js";
import IconButton from "@mui/material/IconButton";
import * as React from "react";
import { MapContainer, TileLayer, Marker } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';

function DetailsBox({data}) {

    const latitude = data.Latitude; // Replace with your desired latitude
    const longitude = data.Longitude; // Replace with your desired longitude

    return (
        <div className="details-box">
            <div className="building-name">
                <IconButton edge="end" aria-label="comments">
                    <HomeWorkIcon />
                </IconButton>
                <div>{data.PropertyName}</div>
            </div>
            <div>Type : {data.PrimaryPropertyType}</div>
            <div>Address: {data.Address}</div>
            <div>Number of floor: {data.NumberofFloors}</div>
            <div>District: {data.CouncilDistrictCode}</div>
            <div>Build in {data.YearBuilt}</div>
            <div>
                <MapContainer center={[latitude, longitude]} zoom={13} style={{ height: '300px', width: '85%' }}>
                    <TileLayer
                        attribution=""
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={[latitude, longitude]} />
                </MapContainer>
            </div>
        </div>
    );
}

export default DetailsBox;