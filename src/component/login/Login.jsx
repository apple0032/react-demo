import {useEffect, useState} from "react";
import {useDispatch} from "react-redux";
import {userLogin} from "../../redux/features/authSlice";
import './login.css'
import {authorizedUser} from "../../services/userService.js"
import {Navigate} from "react-router-dom";
import clientWithHeader from "../../../src/config/apiSetting"

const Login = () => {
  const [input, setInput] = useState({ userName: "", userPassword: "" });
  const dispatch = useDispatch();
  const [error, setError] = useState('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const authorization  = async () => {
    try {
      const isAuthorizedUser = await authorizedUser();
      setIsLoggedIn(isAuthorizedUser);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    authorization();
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();

    // Perform form validation
    const validationError = validateValues(input.userName, input.userPassword);

    if (validationError) {
      setError(validationError);
      return;
    }

    // API request to authenticate the user

    let result = await clientWithHeader.post('/auth/login', JSON.stringify({ "username": input.userName, "password" : input.userPassword }));

    if(result.status === 200){
      if(!result.data.error_message) {
        input.jwt = result.data.access_token
        localStorage.setItem('token', result.data.access_token);
        await dispatch(userLogin(input));
        authorization();
        setError('');
      }else {
        setError("Couldn't find your account.");
      }
    } else {
      setError("Something wrong with our server!");
    }

  };

  function handleChange(event) {
    let name = event.target.name;
    let value = event.target.value;
    setInput((values) => ({ ...values, [name]: value }));
  }

  const validateValues = (username, password) => {

    if (username.trim() === '' || password.trim() === '') {
      return 'Username and password are required.';
    }

    if (username.length > 15 || username.length < 5) {
      return 'Username length should between 5 to 15';
    }
    if (password.password > 15 || password.length < 5) {
      return 'Password length should between 5 to 15';
    }

    return '';
  };

  if (isLoggedIn ) {
    return <Navigate to="/" />;
  }

  return (
      <div className="login-page">
        <div className="form">
          <img src="/company.jpg" alt="Company Logo" />
          {!isLoggedIn ?
            <form className="login-form" onSubmit={handleSubmit}>
              <input
                  type="text"
                  placeholder="User Name"
                  name="userName"
                  value={input.userName}
                  onChange={handleChange}
              />
              <input
                  type="password"
                  placeholder="Password"
                  name="userPassword"
                  value={input.userPassword}
                  onChange={handleChange}
              />
              <button type="submit">Login</button>
              {error && <div className="error-message">{error}</div>}
            </form>
            :
              <div>
                <div className="logged_in">You had been Logged in.</div>
                <button type="submit">Sign Out</button>
              </div>
          }
        </div>
      </div>
  );
};

export default Login;
