
const Header = () => {

    return (
        <div className="header">
            <div className="header-container">
                <img src="/logo.png" alt="Company Logo" />
            </div>
            <div className="header-appName">
                <span>SEATTLE BUILDING DATA VISUALIZATION</span>
            </div>
        </div>
    );

};

export default Header;
