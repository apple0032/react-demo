import { createSlice } from "@reduxjs/toolkit";

const initialState = { isLogin: false };

const authSlice = createSlice({
  name: "authSlice",
  initialState,
  reducers: {
    userLogin: (state, action) => {
      let jwt = action.payload.jwt
      localStorage.setItem('token', jwt);
      state.isLogin = true;
    },
  },
});

export const { userLogin } = authSlice.actions;
export default authSlice.reducer;
