import { BrowserRouter, Route, Routes } from "react-router-dom";
import Overview from "./pages/Overview.jsx";
import ChartPage from "./pages/ChartPage.jsx";
import Header from "./component/header/Header";
import Login from "./component/login/Login";
import PrivateRoute from "./component/protectedRoute/PrivateRoute";
import {Navigate} from "react-router-dom";
const AppRouter = () => {

  return (
    <div>
      <BrowserRouter>
        <Header />
        <div>
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route
              path="/"
              element={
                  <PrivateRoute>
                      <Overview />
                  </PrivateRoute>
              }
            />
            <Route
              path="/chart"
              element={
                <PrivateRoute>
                  <ChartPage />
                </PrivateRoute>
              }
            />
            <Route path="*" element={<Navigate to="/" replace />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
};

export default AppRouter;
