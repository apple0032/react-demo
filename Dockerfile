# syntax=docker/dockerfile:1
########################################################################################################################
# react-demo build stage
########################################################################################################################

# Use an official Node.js runtime as the base image
FROM node:20-alpine AS build

# Set the working directory in the container
WORKDIR /app

# Copy package.json and yarn.lock to the container
COPY package*.json yarn.lock ./

# Install project dependencies
RUN yarn install

# Copy the entire project directory to the container
COPY . .

# Copy the .env file to the container
COPY .env .env

# Build the React app
RUN yarn build

########################################################################################################################
# react-demo image
########################################################################################################################

FROM nginx:stable-alpine
ENV TZ=Asia/Hong_Kong
COPY --from=build /app/dist /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
